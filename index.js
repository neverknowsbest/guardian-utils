module.exports = function GuardianUtils(mod) {

	let entered = false,
		hooks = [];

	// unloads when leaving guardian area or going to lobby
	mod.game.me.on('change_zone', (zone, quick) => { if (entered) unload(); });
	mod.hook('C_RETURN_TO_LOBBY', 'raw', () => {  if (entered) unload(); });

	// loads upon entering guardian area
	mod.hook('S_FIELD_EVENT_ON_ENTER', 'raw', () => {
		if (mod.settings.enabled) {
			load();
			if (mod.settings.disableUI) return false
		}
	});

	// automatically claim rewards
	function claimReward() {
		mod.send('C_REQUEST_FIELD_POINT_REWARD', 1, {})
		setTimeout(() => {
			mod.send('C_REQUEST_ONGOING_FIELD_EVENT_LIST', 1, {})
		}, 2000 + Math.random() * 500)
	};

	function load(){
		entered = true;

		// Show progress on chat
        hook('S_FIELD_POINT_INFO', 2, (event) => {       
			// entered GM, updated progress, unclaimed rewards
            if(entered && event.cleared != clr && event.cleared - 1 > event.claimed){
                mod.toClient('S_CHAT', 3, {
                    channel: 21,
                    gm: 1,
                    name: 'Guardian Mission',
                    message: String(`${event.cleared} / ${event.maxPoints}`)
                });
            }
            clr = event.cleared;
        });

		// auto redeem
		hook('S_FIELD_EVENT_PROGRESS_INFO', 1, () => {
			if (mod.settings.autoRedeem) setTimeout(
				claimReward, 2000 + Math.random() * 250
			);
		});

		// no AFK kick
		hook('C_FIELD_EVENT_BAN', 1, function(event) {
			if (mod.setting.allowAFK) return false;
		});
	}

    mod.command.add("gl", {
        $none(){
			mod.settings.enabled = !mod.settings.enabled;
            mod.command.message(mod.settings.enabled);
        },
        auto(){
			mod.settings.autoRedeem = !mod.settings.autoRedeem;
            mod.command.message(mod.settings.autoRedeem);
        },
        afk(){
			mod.settings.allowAFK = !mod.settings.allowAFK;
            mod.command.message(mod.settings.allowAFK);
        },
        noui(){
			mod.settings.disableUI = !mod.settings.disableUI;
            mod.command.message(mod.settings.disableUI);
			if (entered) mod.command.message("Note: You must re-enter the area for this option to work!")
        },
		help(){
            mod.command.message("gl: Toggle mod");
            mod.command.message("gl auto: Toggle auto redeem");
            mod.command.message("gl afk: Prevent getting kicked from guardian area");
            mod.command.message("gl noui: Disable UI and auto optimization");
        }

    });

	function hook() { hooks.push(mod.hook(...arguments)); }
	function unload() {
        if(hooks.length) {
            for(let h of hooks) {
                mod.unhook(h);
            }
            hooks = []
        }
		entered = false;
    }

    // reload support
    this.destructor = () => {
        mod.command.remove('gl');
    }
    this.saveState = () => {
        mod.log('user called reload, saving state')

        return {
            enabled: enabled,
			entered: entered,
			hooks: hooks
        };
    }
    this.loadState = (state) => {
        mod.log('loading state');

        enabled = state.enabled;
		entered = state.entered;
		hooks = state.hooks;
    }
}

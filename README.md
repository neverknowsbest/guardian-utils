# guardian-utils

QoL pack for Guardian Missions. Features are:

- Auto claim rewards
- Disable UI and auto optimization
- Prevent AFK kicking

# TODO

- Better readme, i'm too lazy to write it right now

# Credits

- Owyn: no\_more\_guardian (disable guardian UI, print points in chat)
- PsykoDev: Auto-Guildquest (auto redeem rewards)
- Saberawr: guardian-afk (prevents AFK teleport)